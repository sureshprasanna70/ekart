class Cart < ApplicationRecord
	has_many :cart_products
	validate :user_or_session_id

	def cart_value
		return ActiveRecord::Base.connection.execute("select COALESCE(SUM((cart_products.product_amount * cart_products.quantity) - discount_amount),0) AS cart_value from carts JOIN cart_products ON carts.id = cart_products.cart_id where cart_id = #{self.id}").entries[0]
	end
	def product_value
		return ActiveRecord::Base.connection.execute("select COALESCE(SUM((cart_products.product_amount * cart_products.quantity)),0) AS product_amount from carts JOIN cart_products ON carts.id = cart_products.cart_id where cart_id = #{self.id}").entries[0]
	end
	def product_discount
		return ActiveRecord::Base.connection.execute("select COALESCE(SUM((cart_products.discount_amount)),0) AS discount_amount from carts JOIN cart_products ON carts.id = cart_products.cart_id where cart_id = #{self.id}").entries[0]
	end

	private
	def user_or_session_id
		return if user_id.present? || session_id.blank?

		if user.id.blank? && session_id.blank?
			errors.add("user_id or session_id", "must be present")
		end
	end
	
end
