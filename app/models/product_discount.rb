class ProductDiscount < ApplicationRecord
	belongs_to :product
	validates_presence_of :start_date
	validates_presence_of :end_date
	validates_presence_of :product_id
	validates_presence_of :discount_price
	validate :end_date_after_start_date
	
	private	
	def end_date_after_start_date
		return if end_date.blank? || start_date.blank?

		if end_date < start_date
			errors.add(:end_date, "must be after the start date")
		end
	end
end
