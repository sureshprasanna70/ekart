class CartProduct < ApplicationRecord
  belongs_to :cart
  belongs_to :product
  validates_presence_of :cart_id
  validates_presence_of :product_id

  after_save :apply_product_discount

  def apply_product_discount
    product_id = self.product_id
    qty = self.quantity
    product = Product.find(product_id)
    product_discount = ProductDiscount.where("product_id = ? AND start_date <= ? AND end_date >= ?", product_id, Date.today, Date.today).first
    if not product_discount.nil?
      if self.quantity%product_discount.quantity == 0
        puts "has discount and updating discount amount"
        multiplier = self.quantity / product_discount.quantity
        self.discount_amount = (self.product_amount - product_discount.discount_price) * multiplier
      end
    end
  end
end
