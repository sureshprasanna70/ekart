class CartDiscount < ApplicationRecord
	validates_presence_of :cart_value
	validates_presence_of :discount_amount
	validates_presence_of :start_date
	validates_presence_of :end_date
	validate :end_date_after_start_date
	validate :cart_value_discount_amount

	private
	def cart_value_discount_amount
		if self.cart_value.present? && self.discount_amount
			if discount_amount < cart_value
				return
			elsif discount_amount > cart_value
				errors.add(:discount_amount,"can't be greater than cart_value")
			end
		end
	end

	def end_date_after_start_date
		return if end_date.blank? || start_date.blank?

		if end_date < start_date
			errors.add(:end_date, "must be after the start date")
		end
	end
end
