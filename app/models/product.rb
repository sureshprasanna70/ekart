class Product < ApplicationRecord
  has_and_belongs_to_many :categories
  has_many :cart_products
  has_many :product_discounts
  accepts_nested_attributes_for :categories
  validates_presence_of :name
  validates_presence_of :price
  validates_presence_of :sku
  validates :sku, :uniqueness => {:case_sensitive => false}
  validates :price, :numericality => { :greater_than_or_equal_to => 0 }
  validates :special_price, :numericality => { :greater_than_or_equal_to => 0 }
  validates :quantity, :numericality => { :greater_than_or_equal_to => 0 }
  validates :weight, :numericality => { :greater_than_or_equal_to => 0 }
  validate :end_date_after_start_date

  private
  def end_date_after_start_date
    return if end_date.blank? || start_date.blank?

    if end_date < start_date
      errors.add(:end_date, "must be after the start date")
    end
  end
end
 
