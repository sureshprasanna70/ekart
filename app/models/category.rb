class Category < ApplicationRecord
  has_many :category_products
  has_many :products, through: :category_products
  has_and_belongs_to_many :products
  accepts_nested_attributes_for :products
  validates_presence_of :name
  validates :name, :uniqueness => {:case_sensitive => false}
end
