class ProductDiscountsController < ApplicationController
  before_action :set_product_discount, only: [:show, :update, :destroy]

  # GET /product_discounts
  def index
    @product_discounts = ProductDiscount.all

    render json: @product_discounts
  end

  # GET /product_discounts/1
  def show
    render json: @product_discount
  end

  # POST /product_discounts
  def create
    @product_discount = ProductDiscount.new(product_discount_params)

    if @product_discount.save
      render json: @product_discount, status: :created, location: @product_discount
    else
      render json: @product_discount.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /product_discounts/1
  def update
    if @product_discount.update(product_discount_params)
      render json: @product_discount
    else
      render json: @product_discount.errors, status: :unprocessable_entity
    end
  end

  # DELETE /product_discounts/1
  def destroy
    @product_discount.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_discount
      @product_discount = ProductDiscount.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_discount_params
      params.require(:product_discount).permit(:product_id, :quantity, :discount_price, :start_date, :end_date)
    end
end
