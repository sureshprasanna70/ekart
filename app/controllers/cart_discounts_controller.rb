class CartDiscountsController < ApplicationController
  before_action :set_cart_discount, only: [:show, :update, :destroy]

  # GET /cart_discounts
  def index
    @cart_discounts = CartDiscount.all

    render json: @cart_discounts
  end

  # GET /cart_discounts/1
  def show
    render json: @cart_discount
  end

  # POST /cart_discounts
  def create
    @cart_discount = CartDiscount.new(cart_discount_params)

    if @cart_discount.save
      render json: @cart_discount, status: :created, location: @cart_discount
    else
      render json: @cart_discount.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /cart_discounts/1
  def update
    if @cart_discount.update(cart_discount_params)
      render json: @cart_discount
    else
      render json: @cart_discount.errors, status: :unprocessable_entity
    end
  end

  # DELETE /cart_discounts/1
  def destroy
    @cart_discount.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cart_discount
      @cart_discount = CartDiscount.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cart_discount_params
      params.require(:cart_discount).permit(:cart_value, :discount_amount, :start_date, :end_date)
    end
end
