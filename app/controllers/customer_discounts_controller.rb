class CustomerDiscountsController < ApplicationController
  before_action :set_customer_discount, only: [:show, :update, :destroy]

  # GET /customer_discounts
  def index
    @customer_discounts = CustomerDiscount.all

    render json: @customer_discounts
  end

  # GET /customer_discounts/1
  def show
    render json: @customer_discount
  end

  # POST /customer_discounts
  def create
    @customer_discount = CustomerDiscount.new(customer_discount_params)

    if @customer_discount.save
      render json: @customer_discount, status: :created, location: @customer_discount
    else
      render json: @customer_discount.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /customer_discounts/1
  def update
    if @customer_discount.update(customer_discount_params)
      render json: @customer_discount
    else
      render json: @customer_discount.errors, status: :unprocessable_entity
    end
  end

  # DELETE /customer_discounts/1
  def destroy
    @customer_discount.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer_discount
      @customer_discount = CustomerDiscount.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def customer_discount_params
      params.require(:customer_discount).permit(:customer_group_id, :discount_amount, :start_date, :end_date)
    end
end
