class CheckoutController < ApplicationController

	def index
		cart = Cart.find(params[:cart_id])
		checkout = Hash.new
		if not cart.nil?
			cart_value = cart.cart_value.sum
			cart_discount = CartDiscount.where('cart_value <= ? AND start_date <= ? AND end_date >= ?', cart_value,Date.today,Date.today).order('discount_amount DESC').limit(1)
			puts cart_discount.inspect
			checkout[:cart_value] = cart_value
			checkout[:cart_items] = cart.cart_products.to_a
			checkout[:product_amount] = cart.product_value[0]
			checkout[:product_discount] = cart.product_discount[0]
			checkout[:net_amount] = cart_value
			checkout[:customer_discount] = "Yet to be implemented"
			if cart_discount.size > 0
				checkout[:cart_discount] = cart_discount.first.discount_amount
				checkout[:net_amount] = cart_value - checkout[:cart_discount]
			end
			render json: checkout
		end
	end
end
