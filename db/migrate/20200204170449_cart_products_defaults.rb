class CartProductsDefaults < ActiveRecord::Migration[5.2]
  def change
   change_column :cart_products, :quantity, :integer, :default => 1
   change_column :cart_products, :product_amount, :decimal, :default => 0
   change_column :cart_products, :discount_amount, :decimal, :default => 0
   change_column :product_discounts, :quantity, :integer, :default => 2
  end
end
