class CreateProductDiscounts < ActiveRecord::Migration[5.2]
  def change
    create_table :product_discounts do |t|
      t.integer :product_id
      t.integer :quantity
      t.decimal :discount_price
      t.datetime :start_date
      t.datetime :end_date

      t.timestamps
    end
  end
end
