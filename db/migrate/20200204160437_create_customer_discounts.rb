class CreateCustomerDiscounts < ActiveRecord::Migration[5.2]
  def change
    create_table :customer_discounts do |t|
      t.integer :customer_group_id
      t.decimal :discount_amount
      t.datetime :start_date
      t.datetime :end_date

      t.timestamps
    end
  end
end
