class ChangeCategoryProductsToCategoriesProducts < ActiveRecord::Migration[5.2]
  def change
  	rename_table :category_products, :categories_products
  end
end
