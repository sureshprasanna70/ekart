class ProductAttributeDefaults < ActiveRecord::Migration[5.2]
  def change
     change_column :products, :quantity, :integer, default: 0
     change_column :products, :weight, :decimal, default: 0
     change_column :products, :visibility, :boolean, default: true
     change_column :products, :price, :decimal, default: true
  end
end
