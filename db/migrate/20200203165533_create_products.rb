class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.decimal :price
      t.decimal :special_price
      t.datetime :start_date
      t.datetime :end_date
      t.string :sku
      t.integer :quantity
      t.boolean :visibility
      t.integer :weight

      t.timestamps
    end
  end
end
