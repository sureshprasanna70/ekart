class IndexesAndKeys < ActiveRecord::Migration[5.2]
  def change
    add_index :categories, :name, unique: true
    add_index :products, :sku, unique: true
  end
end
