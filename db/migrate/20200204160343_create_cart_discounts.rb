class CreateCartDiscounts < ActiveRecord::Migration[5.2]
  def change
    create_table :cart_discounts do |t|
      t.decimal :cart_value
      t.decimal :discount_amount
      t.datetime :start_date
      t.datetime :end_date

      t.timestamps
    end
  end
end
