Rails.application.routes.draw do
  resources :customer_discounts
  resources :cart_discounts
  resources :product_discounts
  resources :cart_products
  resources :carts
  resources :category_products
  resources :products
  resources :categories

  get 'checkout/:cart_id' => "checkout#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
