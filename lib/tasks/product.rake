require 'digest'
namespace :product do
  desc "TODO"
  task build_sample_data: :environment do
    (1..5).each do |num|
       category = Category.create!(name: "Category#{num}")
       (1..10).each do |product_num|
          product = Product.new()
          product.name = Faker::Lorem.unique.words(number: 1)
          product.description = Faker::Lorem.unique.paragraph
          product.price = Faker::Commerce.price(range: 10..100)
          product.special_price = 0 
          product.quantity = Faker::Number.between(from: 1, to: 20)
          product.sku = Digest::SHA2.hexdigest product.name + product.description
          product.category_ids = [category.id]
          product.save!
          
       end
    end
    puts "Sample data loaded"
  end

end
