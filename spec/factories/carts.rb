FactoryBot.define do
  factory :cart do
    user_id { 1 }
    session_id { 1 }
    purchased { false }
  end
end
