FactoryBot.define do
  factory :cart_discount do
    cart_value { "9.99" }
    discount_amount { "2" }
    start_date { Date.today }
    end_date { Date.today + 2 }
  end
end
