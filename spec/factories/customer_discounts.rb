FactoryBot.define do
  factory :customer_discount do
    customer_group_id { 1 }
    discount_amount { "9.99" }
    start_date { "2020-02-04 21:34:37" }
    end_date { "2020-02-04 21:34:37" }
  end
end
