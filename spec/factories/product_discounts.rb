FactoryBot.define do
  factory :product_discount do
    product
    quantity { 1 }
    discount_price { "1" }
    start_date { Date.today }
    end_date { Date.today + 2 }
  end
end
