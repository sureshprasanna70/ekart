FactoryBot.define do
  factory :cart_product do
    cart_id { 1 }
    product_id { 1 }
    product_amount { "9.99" }
    discount_amount { "9.99" }
    quantity { 1 }
  end
end
