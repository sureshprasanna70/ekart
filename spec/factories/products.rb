FactoryBot.define do
  factory :product do
    name {Faker::Commerce.product_name} 
    price {Faker::Commerce.price(range: 0..10.0)}
    sku {"#{Faker::Lorem.words(number: 1)}#{rand 1000}"}
    quantity {Faker::Number.between(from: 10, to: 20)}
    special_price {Faker::Number.between(from: 0, to: 1)}
    weight {Faker::Number.between(from: 1, to: 10)}
    categories { FactoryBot.create_list(:category, 5) }
  end
end
