require 'rails_helper'

RSpec.describe "ProductDiscounts", type: :request do
  let(:product_discounts) { create_list(:product_discounts,10)}
  let(:product) { create(:product)}

  describe "GET /product_discounts" do
    it "works! (now write some real specs)" do
      get product_discounts_path
      expect(response).to have_http_status(200)
    end
  end

  describe "POST /product_discounts" do
    context "valid cases" do
  let(:product) { create(:product)}

      let(:valid_attributes) { {product_discount: {product_id: product.id, discount_price: 1, start_date: Date.today, end_date: Date.today}}}
      it "create product discount" do
        post product_discounts_path, params: valid_attributes
        expect(response).to have_http_status(201)
      end
    end
    context "invalid cases" do
      let(:invalid_attributes) { {product_discount: {product_id: product.id, discount_price: 20, start_date: Date.today + 2, end_date: Date.today}}}
      it "create product discount" do
        post product_discounts_path, params: invalid_attributes
        expect(response).to have_http_status(422)
      end
    end
  end
end
