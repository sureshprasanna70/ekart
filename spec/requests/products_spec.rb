require 'rails_helper'

RSpec.describe 'Products API', type: :request do
  let!(:product) { create_list(:product, 10) }

  describe 'GET /products' do
    before { get '/products' }

    it 'returns products' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end
    describe 'POST /products' do
    let(:valid_attributes) { {product: { name: 'Clothing',price: 12, sku:"821379821",quantity: 10, weight: 10, special_price: 10
     }} }

    context 'when the request is valid' do
      before { post '/products', params: valid_attributes }

      it 'creates a product' do
        expect(json['name']).to eq('Clothing')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end
  end
end
