require 'rails_helper'

RSpec.describe "Carts", type: :request do
	let(:carts) { create_list(:cart, 10)}
  describe "GET /carts" do
    it "works! (now write some real specs)" do
      get carts_path
      expect(response).to have_http_status(200)
    end
  end

  describe "POST /carts" do
  	let(:valid_attributes) { {cart:{user_id: 1}}}
    it "works! (now write some real specs)" do
      post carts_path, params: valid_attributes
      expect(response).to have_http_status(201)
    end
  end
end
