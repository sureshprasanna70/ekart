require 'rails_helper'

RSpec.describe "CartDiscounts", type: :request do
  describe "GET /cart_discounts" do
    it "works! (now write some real specs)" do
      get cart_discounts_path
      expect(response).to have_http_status(200)
    end
  end

  describe "POST /cart_discounts" do
    context "valid cases" do
      let(:valid_attributes) { {cart_discount: {cart_value: 100, discount_amount: 20, start_date: Date.today, end_date: Date.today}}}
      it "create cart discount" do
        post cart_discounts_path, params: valid_attributes
        expect(response).to have_http_status(201)
      end
    end
    context "invalid cases" do
      let(:invalid_attributes) { {cart_discount: {cart_value: rand(100), discount_amount: 20, start_date: Date.today + 2, end_date: Date.today}}}
      it "create cart discount" do
        post cart_discounts_path, params: invalid_attributes
        expect(response).to have_http_status(422)
      end
    end
  end
end
