require 'rails_helper'

RSpec.describe "CartProducts", type: :request do
	let(:product) { create(:product) }
	let(:cart) { create(:cart) }
	let(:cart_products) { create_list(:cart_products, 10, cart: cart, product: product) }

	describe "GET /cart_products" do
		it "works!" do
			get cart_products_path
			expect(response).to have_http_status(200)
		end
	end

	describe "POST /cart_products" do
		let(:valid_attributes) { {cart_product:{cart_id: cart.id, product_id: product.id }}}
		it "works!" do
			post cart_products_path, params: valid_attributes
			expect(response).to have_http_status(201)
		end
	end
end
