require 'rails_helper'

RSpec.describe 'Categories API', type: :request do
  let!(:categories) { create_list(:category, 10) }
  let!(:category_id) { categories.first.id }

  describe 'GET /categories' do
    before { get '/categories' }

    it 'returns categories' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'POST /categories' do
    let(:valid_attributes) { {category: { name: 'Clothing' }} }

    context 'when the request is valid' do
      before { post '/categories', params: valid_attributes }

      it 'creates a category' do
        expect(json['name']).to eq('clothing')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end
  end
end
