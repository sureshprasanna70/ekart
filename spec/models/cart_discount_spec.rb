require 'rails_helper'

RSpec.describe CartDiscount, type: :model do
  it { should validate_presence_of(:cart_value) }
  it { should validate_presence_of(:discount_amount) }
  it { should validate_presence_of(:start_date) }
  it { should validate_presence_of(:end_date) }
end
