require 'rails_helper'

RSpec.describe Product, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:sku) }
  it { should validate_uniqueness_of(:sku).ignoring_case_sensitivity }
  it { should validate_presence_of(:price) }
  it { should validate_numericality_of(:price).is_greater_than_or_equal_to(0) }
  it { should validate_numericality_of(:special_price).is_greater_than_or_equal_to(0) }
  it { should have_and_belong_to_many(:categories) }
  it { should have_many(:product_discounts) }
end
