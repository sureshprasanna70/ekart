require 'rails_helper'

RSpec.describe ProductDiscount, type: :model do

	it {should belong_to(:product)}
	it {should validate_presence_of(:product_id)}
	it {should validate_presence_of(:discount_price)}
	it {should validate_presence_of(:start_date)}
	it {should validate_presence_of(:end_date)}
end
