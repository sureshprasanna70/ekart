require 'rails_helper'

RSpec.describe CartProduct, type: :model do
  it { should belong_to(:product) }
  it { should validate_presence_of(:product_id) }
  it { should validate_presence_of(:cart_id) }
end
