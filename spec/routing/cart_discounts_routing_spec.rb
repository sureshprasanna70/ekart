require "rails_helper"

RSpec.describe CartDiscountsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/cart_discounts").to route_to("cart_discounts#index")
    end

    it "routes to #show" do
      expect(:get => "/cart_discounts/1").to route_to("cart_discounts#show", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/cart_discounts").to route_to("cart_discounts#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/cart_discounts/1").to route_to("cart_discounts#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/cart_discounts/1").to route_to("cart_discounts#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cart_discounts/1").to route_to("cart_discounts#destroy", :id => "1")
    end
  end
end
