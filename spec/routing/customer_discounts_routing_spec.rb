require "rails_helper"

RSpec.describe CustomerDiscountsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/customer_discounts").to route_to("customer_discounts#index")
    end

    it "routes to #show" do
      expect(:get => "/customer_discounts/1").to route_to("customer_discounts#show", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/customer_discounts").to route_to("customer_discounts#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/customer_discounts/1").to route_to("customer_discounts#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/customer_discounts/1").to route_to("customer_discounts#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/customer_discounts/1").to route_to("customer_discounts#destroy", :id => "1")
    end
  end
end
